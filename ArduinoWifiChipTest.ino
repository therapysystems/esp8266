

char serialbuffer[1000];//serial buffer for request url

void setup()
{
  Serial1.begin(115200);//connection to ESP8266
  Serial.begin(115200); //serial debug
  
  
  //set mode needed for new boards
  Serial1.println("AT+RST");
  Serial1.println("AT+CWMODE_DEF=1");
  delay(5000);//delay after mode change
  Serial1.println("AT+RST");
  
  //connect to wifi network
  Serial1.println("AT+CWJAP_DEF=\"Dylan's iPhone\",\"dylanmartin\"");
  delay(5000);
  // check to see if connected
  Serial1.println("AT+CWLAP");
  Serial.println("Reading...");
   while (Serial1.available() > 0) {
    Serial.write(Serial1.read());
  }
  
  Serial.println("FINISHED SETUP"); 
  
}

void loop()
{
  //output everything from ESP8266 to the Arduino Micro Serial output
  while (Serial1.available() > 0) {
    Serial.write(Serial1.read());
  }
//  Serial.println("end write");
  if (Serial.available() > 0) {
     Serial.println("Done Reading");
     //read from serial until terminating character
     int len = Serial.readBytesUntil('\n', serialbuffer, sizeof(serialbuffer));
  
     //trim buffer to length of the actual message
     String message = String(serialbuffer).substring(0,len-1);
     Serial.println("message: " + message);
 
     //check to see if the incoming serial message is a url or an AT command
     if(message.substring(0,2)=="AT"){
       //make command request
       Serial.println("COMMAND REQUEST");
       Serial1.println(message); 
     }else{
      //make webrequest
       Serial.println("WEB REQUEST");
       WebRequest(message);
     }
  }
}

//web request needs to be sent without the http for now, https still needs some working
void WebRequest(String request){
 //find the dividing marker between domain and path
     int slash = request.indexOf('/');
     
     //grab the domain
     String domain;
     if(slash>0){  
       domain = request.substring(0,slash);
     }else{
       domain = request;
     }

     //get the path
     String path;
     if(slash>0){  
       path = request.substring(slash);   
     }else{
       path = "/";          
     }

     
     //output domain and path to verify
     Serial.println("domain: |" + domain + "|");
     Serial.println("path: |" + path + "|");     
     
     //create start command
     String startcommand = "AT+CIPSTART=\"TCP\",\"173.49.67.11:8080/ \", 80"; //443 is HTTPS, still to do
     
     Serial1.println(startcommand);
     Serial.println(startcommand);

     // status command for debugging

     String statuscommand = "AT+CIPSTATUS 173.49.67.11:6900";
     Serial1.println(statuscommand);
     delay(1000);
     int incomingByte = 0;   // for incoming serial data

     // send data only when you receive data:
     // read the incoming byte:
     incomingByte = Serial1.read();

     // say what you got:
     Serial.print("I received: ");
     Serial.println(incomingByte, DEC);
     Serial.println(statuscommand);
     
     //test for a start error
     if(Serial1.find("Error")){
       Serial.println("error on start");
       return;
     }
     
     int space = request.indexOf(" ");
     String req = request.substring(0,space);
     Serial.println("request is " + request);
     Serial.println("req is " + req);

     //create the request command
//     String sendcommand = "GET http://"+ req;
     String sendcommand = "GET http://us-central1-reasonstodrinkserver.cloudfunctions.net/getHoliday";
     
     Serial.println(sendcommand);
     
     //send 
     Serial1.print("AT+CIPSEND=");
     Serial1.println(sendcommand.length());
     
     //debug the command
     Serial.print("AT+CIPSEND=");
     Serial.println(sendcommand.length());

     
     
     //Serial.print(getcommand);
     Serial1.print(sendcommand); 
     delay(5000);
     // read the incoming byte:
     incomingByte = Serial1.read();

     // say what you got:
     Serial.print("I received: ");
     Serial.println(incomingByte, DEC);
     
}
